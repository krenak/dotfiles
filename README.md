# dotfiles

This repo gathers my current Bash and Vim dotfiles. 
Feel free to propose any changing, especially improvements, to them. Futher instructions on how to get access, modify and send me back are descripted below.

## Colaborating

It is quite simple to "copy" these files once you get the git basics of clonning, forking and so on.
For theses files, it's recommended to clone the entire repository, as one will quickly get access of all files. To do so, type in your shell:

```git clone git@gitlab.com:krenak/dotfiles.git```

Once the process is complete, you can go to the ```dotfile/``` directory and start to work on the files.

As an alternative and fast option, you can [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the entire repository and contribute after getting it.

## Modifying files

Also, it's recommended some degree of Bash, Shell and Vim scripts knowledge to modify all files. For personalizations such customizing Bash Prompt, [some sources](https://scriptim.github.io/bash-prompt-generator/) are more reliable and fast than just go through all Bash/Shell documentation.
If otherwise, and to improve your knowledge in these two canonical and fundamental languages, take a look at [Bash](https://www.gnu.org/software/bash/manual/bash.pdf) documentation as well as [Git](https://git-scm.com/book/en/v2) to acelerate your contributions.

## Getting in touch

For any inquires and futher discutions, please feel free to send me an [email](mailto:kaspa@onionmail.org) or post commentaries in my [personal blog](andyfraga.info).
