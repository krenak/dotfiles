
let g:livepreview_cursorhold_recompile = 0 "preventing compilation on cursor hold
autocmd Filetype tex setl updatetime=1
let g:livepreview_use_biber = 1 "preventing biber use
let g:livepreview_engine = 'pdflatex'
let g:livepreview_previewer = 'evince'

filetype plugin indent on
syntax on
set title

" lines
" shows line numbers at the left column
set number

" replace tabs with spaces
" set expandtab

" 1 tab = 2 spaces
set tabstop=8 softtabstop=8 shiftwidth=8 noexpandtab

" when creating a new line, copy the indentation from the line above
" 1 tab of worth of space (for us this is 2 spaces)
set smarttab

" when creating a new line, copy the indentation from the line above
set autoindent

" search tool
" ignore case when searching

set ignorecase
set smartcase

" highlight search results
set hlsearch

" highlight all pattern matches WHILE typing the pattern 
set incsearch

" Mix
" highlight current line
" set cursorline

call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"   Plug 'junegunn/vim-easy-align'
"
"   " Any valid git URL is allowed
"   Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"
"   " Multiple Plug commands can be written in a single line using | separators
"   Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
"
"   " On-demand loading
"   Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"   Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
"
"   " Using a non-default branch
"   Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
"
"   " Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"   Plug 'fatih/vim-go', { 'tag': '*' }
"
"   " Plugin options
"   Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
"
"   " Plugin outside ~/.vim/plugged with post-update hook
"   Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"
"   " Unmanaged plugin (manually installed and updated)
"   Plug '~/my-prototype-plugin'
    Plug 'skywind3000/asyncrun.vim'
    Plug 'xuhdev/vim-latex-live-preview'
" Plugin for Bracey Live Server in Vim
    Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}
"   " Initialize plugin system
call plug#end()
